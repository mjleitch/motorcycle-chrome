// tslint:disable no-expression-statement
// tslint:disable no-console

// export * from './lib/async';
// export * from './lib/hash';
// export * from './lib/number';

// const SEARCH_SELECTOR = `#search_term`;
const URL = `https://www.zealoptics.com/`;

import { launch } from 'puppeteer';

(async () => {
  const browser = await launch();

  const page = await browser.newPage();
  await page.goto(URL, { waitUntil: 'domcontentloaded' });
  // await page.waitForSelector(SEARCH_SELECTOR);
  await page.screenshot({ path: 'example.png' });

  await browser.close();
})();