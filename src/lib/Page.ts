// tslint:disable no-expression-statement
// tslint:disable interface-over-type-literal
import { chain, drain, fromPromise, hold, sampleWith, take, tap } from '@motorcycle/stream'
import { Stream } from '@motorcycle/types'
import { Browser, launch } from 'puppeteer'
import { fromEvent } from './fromEvent'



export interface ChromeSources {
    // readonly defaultArgs: () => Stream<Array<string>>
    // readonly executablePath: () => Stream<string>
    readonly events: () => Stream<any>
}

export type ChromeSinks = { readonly close$: Stream<any> }

const close = (b: Browser) => b.close()

export function makeBrowserComponent(options: {}): (c: ChromeSinks) => ChromeSources {
    const browser = launch(options);




    return function BrowserComponent(sinks: ChromeSinks): ChromeSources {
        const { close$ } = sinks;
        const browser$ = fromPromise(browser);

        const disconnected$ = hold(chain((b: Browser) => fromEvent('disconnected', b), browser$))

        const closeEff$ = tap(close, sampleWith(close$, browser$))

        drain(take(1, closeEff$))
        return {
            events: () => disconnected$
        }
    }
}

