import { currentTime } from '@most/scheduler'
import { Scheduler, Sink } from '@motorcycle/types'
import { Browser } from 'puppeteer'

export const fromEvent = (event: string, target: Browser) => ({
  run: <T>(sink: Sink<T>, scheduler: Scheduler) => {
    const send = () => tryEvent(currentTime(scheduler), event, sink)
    const dispose = () => target.removeListener(event, send)

    target.addListener(event, send)

    return { dispose }
  }
})

export function tryEvent<S>(t: number, x: any, sink: Sink<S>) {
  try {
    sink.event(t, x)
  } catch (e) {
    sink.error(t, e)
  }
}





// import { StandardEvents } from '../'
