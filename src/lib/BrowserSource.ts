// tslint:disable no-this-assignment
// tslint:disable max-classes-per-file
// tslint:disable interface-over-type-literal

import { drain, filter, fromPromise, hold, map, multicast, sampleWith, switchLatest, tap } from '@motorcycle/stream'
import { Disposable, IOComponent, Scheduler, Sink, Stream } from '@motorcycle/types'
import { pipe } from '@typed/prelude';

import { Browser, launch } from 'puppeteer'
export type BrowserEvents = 'disconnected'
    | 'targetchanged'
    | 'targetcreated'
    | 'targetdestroyed'

// event: 'disconnected'
// Emitted when Puppeteer gets disconnected from the Chromium instance. This might happen because of one of the following:

// Chromium is closed or crashed
// The browser.disconnect method was called
// event: 'targetchanged'
// <Target>
// Emitted when the url of a target changes.

// NOTE This includes target changes in incognito browser contexts.

// event: 'targetcreated'
// <Target>
// Emitted when a target is created, for example when a new page is opened by window.open or browser.newPage.

// NOTE This includes target creations in incognito browser contexts.

// event: 'targetdestroyed'
// <Target>
// Emitted when a target is destroyed, for example when a page is closed.

// NOTE This includes target destructions in incognito browser contexts.

export class BrowserSource {
    private readonly browser$: Stream<Browser>
    private readonly eventMap: Map<BrowserEvents, Stream<any>>

    constructor(browser$: Stream<Browser>) {
        this.browser$ = browser$;
        this.eventMap = new Map();
    }

    public events(
        eventType: BrowserEvents
    ): Stream<any> {
        console.log('events')
        const { eventMap, browser$ } = this

        if (eventMap.has(eventType)) return eventMap.get(eventType) as Stream<BrowserEvents>
        console.log('events2')
        const createEventStream = pipe(
            map((b: Browser) => new EventStream(eventType, b)),
            switchLatest,
            multicast
        )
        const event$ = createEventStream(browser$)

        eventMap.set(eventType, event$)

        return event$
    }
}

export interface BrowserSources {
    // readonly defaultArgs: () => Stream<Array<string>>
    // readonly executablePath: () => Stream<string>
    readonly browser: BrowserSource
}

export type BrowserSinks = { readonly browser$: Stream<any> }

const close = (b: Browser) => b.close()

export function makeBrowserComponent(opts: any): IOComponent<BrowserSinks, BrowserSources> {
    const browser = launch(opts)

    return (sinks: BrowserSinks): BrowserSources => {

        const close$ = hold(filter((i: string) => i === 'disconnected', sinks.browser$))
        const browser$ = hold(fromPromise(browser))

        const effects$ = tap(close, sampleWith(close$, browser$))



        drain(effects$)

        return {
            browser: createBrowserSource(sampleWith(close$, browser$))
        }
    }
}



export class EventStream {
    private readonly eventType: BrowserEvents
    private readonly target: Browser

    constructor(eventType: BrowserEvents, target: Browser) {
        this.eventType = eventType
        this.target = target
    }

    public run(sink: Sink<BrowserEvents>, scheduler: Scheduler): Disposable {
        const { eventType, target } = this

        // Creates an event Handler function that pushes the event data to the stream
        const listener = (event: BrowserEvents) => {
            console.log(event)
            sink.event(scheduler.currentTime(), event)
        }

        // Creates a dispose function to be called when the stream is disposed
        // in this case, the dispose function calls the elements remove listener
        const dispose = () => target.removeListener(eventType, listener)

        // sets up the event listener
        target.addListener(eventType, listener)

        return { dispose }
    }
}


export function createBrowserSource(browser$: Stream<Browser>): BrowserSource {
    return new BrowserSource(browser$)
}

