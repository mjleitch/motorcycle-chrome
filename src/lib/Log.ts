// tslint:disable no-expression-statement
// tslint:disable interface-over-type-literal

import { drain, hold, tap } from '@motorcycle/stream'
import { Stream } from '@motorcycle/types'

// import { prop } from '@typed/prelude'
// import { vNodeWrapper } from './vNodeWrapper'

export type ConsoleSources = void

export type ConsoleSinks = { readonly log$: Stream<string> }

export function makeConsoleComponent(console: Console): (c: ConsoleSinks) => ConsoleSources {
    const log = tap(console.log.bind(console));
    //   const rootVNode = elementToVNode(element)
    //   const wrapVNode = map(vNodeWrapper(element))
    //   const patch = scan(init(), rootVNode)

    return function consoleComponent(sinks: ConsoleSinks): ConsoleSources {
        const { log$ } = sinks;
        const logEff$ = hold(log(log$))

        // const elementVNode$ = patch(wrapVNode(view$))
        // const element$ = hold(toElement(elementVNode$))
        // const dom = createDomSource(element$)

        drain(logEff$);
        return;
    }
}