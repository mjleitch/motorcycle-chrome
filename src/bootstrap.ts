// tslint:disable no-expression-statement
// tslint:disable no-if-statement


import { run } from '@motorcycle/run'
import { makeBrowserComponent } from './lib/BrowserSource'
import { makeConsoleComponent } from './lib/Log'
import { ReorderableList, Sinks, Sources } from './ReorderableList'


const browser = makeBrowserComponent({ headless: false });
const consoleComp = makeConsoleComponent(console)



function IO(sinks: Sinks): Sources {
    const { browser$, log$ } = sinks

    // const list = (['agreeable', 'brave', 'calm', 'delightful', 'eager', 'faithful', 'gentle', 'happy'] as ReadonlyArray<string>)

    // observe(console.log.bind(console), merge(browser$, log$))

    consoleComp({ log$ })

    return {
        ...browser({ browser$ })
    }
}

(async () => {
    await run<Sources, Sinks>(ReorderableList, IO)
})()