// tslint:disable interface-over-type-literal
// import { DomSinks, DomSources } from '@motorcycle/mostly-dom'

import { Stream } from '@motorcycle/types'
import { BrowserSource } from '../lib/BrowserSource';

export interface Sources {
  readonly browser: BrowserSource
}

export type Sinks = {
  readonly browser$: Stream<any>
  readonly log$: Stream<string>
}
