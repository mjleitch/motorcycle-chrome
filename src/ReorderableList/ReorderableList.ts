import { at } from '@motorcycle/stream'
// import { map as mapList, move } from '@typed/prelude'
import { Sinks, Sources } from './types'
// import { periodic } from '../../node_modules/@most/scheduler';
// import { listItem, view } from './view'

export function ReorderableList(sources: Sources): Sinks {
  const { browser } = sources
  const log$ = browser.events('disconnected');
  const browser$ = at(3, 'disconnected');


  return {
    browser$,
    log$
  }
}
